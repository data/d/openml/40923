# OpenML dataset: Devnagari-Script

https://www.openml.org/d/40923

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Prashnna K Gyawali, Shailesh Acharya, Ashok Pant  
**Source**: [Computer Vision Research Group, Nepal](https://web.archive.org/web/20160105230017/http://cvresearchnepal.com/wordpress/dhcd/), [Kaggle](https://www.kaggle.com/rishianand/devanagari-character-set)   
**Please cite**: Acharya S., Pant A.K., Gyawali P.K.,”Deep Learning based large scale handwritten Devanagari Character Recognition” Proceeding of 9th International Conference on Software, Knowledge, Information Management & Applications  

**Character Recognition for Devanagari Script**  

In human civilisation, languages evolved first, and then came scripts. The Devanagari script is one of the oldest scripts of India, having evolved from the ancient Brahmi script. It came to be adopted widely from around the 7th century CE, and is the world’s fourth most used script after Latin, Chinese and Arabic. It is used in nearly 120 languages and dialects, including Nepali, Marathi and, of course, Hindi. It is also the script in which Sanskrit, the language of holy Hindu scriptures, is written. In fact, the word Devanagari means ‘that from the city of gods’. Its distinguishing feature is a horizontal bar drawn at the top to connect the letters forming each word.

This dataset consists of 92 thousand images (32x32 pixels) of 46 characters from Devanagari script. Includes the alphabet as well as the numbers. Devanagari is an Indic script and forms a basis for over 100 languages spoken in India and Nepal including Hindi, Marathi, Sanskrit, and Maithili. It comprises of 47 primary alphabets, 14 vowels, and 33 consonants, and 10 digits. In addition, the alphabets are modified when a vowel is added to a consonant. There is no capitalization of alphabets, unlike Latin languages.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Devanagari_consonants_transp.png/912px-Devanagari_consonants_transp.png)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40923) of an [OpenML dataset](https://www.openml.org/d/40923). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40923/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40923/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40923/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

